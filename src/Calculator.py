from tkinter import *
from functools import partial

root = None
entry_b = None
result_flag = False
last_result = None
ops = {
    '/',
    '*',
    '-',
    '+'
}


def main():
    global root
    root = Tk()
    root.title('Calculator')
    root.geometry("400x300")
    root['bg'] = 'black'

    create_entry()
    create_numpad()
    add_weights()
    root.mainloop()


def create_entry():
    global entry_b, root
    entry_b = Entry(root, bg='grey', bd=3)
    entry_b.grid(row=0, column=0, columnspan=4, sticky='nsew')
    entry_b.bind('<Return>', get_result)
    entry_b.bind('<KP_Enter>', get_result)
    entry_b.bind('<Key>', reset_entry)


def create_numpad():
    global entry_b, root
    num = 1
    for y in range(3):
        for x in range(3):
            b = Button(root, text='%s' % num, command=partial(set_callback, num), bd=5)
            b.grid(row=3 - y, column=x, sticky='nsew')
            b.configure(bg='gray', activebackground='grey40')
            num += 1
    Button(root, text='C', command=lambda: entry_b.delete(0, END), bg='goldenrod', activebackground='dark goldenrod', bd=5, width=1).grid(row=4, column=0, sticky='nsew')
    Button(root, text='0', command=partial(set_callback, 0), bg='grey', activebackground='grey40', bd=5).grid(row=4, column=1, sticky='nsew')
    Button(root, text='=', command=get_result, width=1, bg='DarkOliveGreen3', activebackground='DarkOliveGreen4', bd=5).grid(row=4, column=2, sticky='nsew')
    Button(root, text='/', command=partial(set_callback, '/'), bg='goldenrod', activebackground='dark goldenrod', bd=5, width=2).grid(row=1, column=3, sticky='nsew')
    Button(root, text='*', command=partial(set_callback, '*'), bg='goldenrod', activebackground='dark goldenrod', bd=5, width=2).grid(row=2, column=3, sticky='nsew')
    Button(root, text='-', command=partial(set_callback, '-'), bg='goldenrod', activebackground='dark goldenrod', bd=5, width=2).grid(row=3, column=3, sticky='nsew')
    Button(root, text='+', command=partial(set_callback, '+'), bg='goldenrod', activebackground='dark goldenrod', bd=5, width=2).grid(row=4, column=3, sticky='nsew')


def add_weights():
    global root
    for x in range(5):
        Grid.rowconfigure(root, x, weight=1)
    for x in range(4):
        Grid.columnconfigure(root, x, weight=1)


def get_result(event):
    global entry_b, result_flag, last_result, ops
    current_entry = entry_b.get()
    if len(current_entry) == 0:
        return
    try:
        # I realize this is an extremely unsafe method of achieving this, however this project is a simple 
        # way for me to learn how to use some of the capabilities of tkinter rather than a serious project.
        # As of now, I have no intentions of moving away from the use of eval here.
        result = eval(current_entry if current_entry[0] not in ops else str(last_result) + current_entry)
        result_flag = False
    except:
        print('Not valid expression')
        return
    entry_b.delete(0, END)
    final = result if "." in str(result) and not result.is_integer() else str(result).split('.')[0]
    entry_b.insert(0, final)
    last_result = final


def set_callback(text):
    global entry_b, result_flag
    reset_entry(None)
    entry_b.insert(END, text)


def reset_entry(event):
    global entry_b, result_flag
    if result_flag:
        entry_b.delete(0, END)
        result_flag = False


main()
